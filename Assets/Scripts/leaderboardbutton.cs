﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class leaderboardbutton :MonoBehaviour , IPointerClickHandler
{
	public Sprite signin, signout;
	public GameObject signbutton;

	public void OnPointerClick(PointerEventData data)
	{
		if(Social.localUser.authenticated) {
			PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkI7pbQs9MJEAIQAg");
		} else {
			Social.localUser.Authenticate((bool success) => {

				if(success)
				{
					signbutton.GetComponent<Image>().sprite = signout;
				}else {
					signbutton.GetComponent<Image>().sprite = signin;
				}
			});
		}
	}

	private void Update()
	{

	}
}
