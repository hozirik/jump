﻿using UnityEngine;
using System.Collections;

public class gameJump : MonoBehaviour {

	public GameObject ball;
	//Vector3 screenPoint;
	//Vector3 offset;

	private Vector3 oldMouse;
	private Vector3 mouseSpeed;
	public int speed = 1;
	public static float timescale = 2;

	public GameObject hoveredGO;
	public enum HoverState{HOVER, NONE};
	public HoverState hover_state = HoverState.NONE;

	// Use this for initialization
	void Start () {

		Time.timeScale = timescale;
	}
	
	// Update is called once per frame
	void Update () {
		
		RaycastHit hitInfo = new RaycastHit();
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		if(Physics.Raycast(ray, out hitInfo)){
			if(hover_state == HoverState.NONE){
				hitInfo.collider.SendMessage("OnMouseEnter", SendMessageOptions.DontRequireReceiver);
				hoveredGO = hitInfo.collider.gameObject;
			}
			hover_state = HoverState.HOVER;       
		}
		else{
			if(hover_state == HoverState.HOVER){
				hoveredGO.SendMessage("OnMouseExit", SendMessageOptions.DontRequireReceiver);
			}
			hover_state = HoverState.NONE;
		}

		if(hover_state == HoverState.HOVER){
			hitInfo.collider.SendMessage("OnMouseOver", SendMessageOptions.DontRequireReceiver); //Mouse is hovering
			if(Input.GetMouseButtonDown(0)){
				hitInfo.collider.SendMessage("OnMouseDown", SendMessageOptions.DontRequireReceiver); //Mouse down
			}
			if(Input.GetMouseButtonUp(0)){
				hitInfo.collider.SendMessage("OnMouseUp", SendMessageOptions.DontRequireReceiver); //Mouse up
			}
		}
	}

	void OnMouseDown()
	{
		oldMouse = Input.mousePosition;
		//screenPoint = Camera.main.WorldToScreenPoint(ball.transform.position);
		//offset = ball.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 
		//	Input.mousePosition.y, screenPoint.z));
	}


	void OnMouseDrag()
	{
		/*
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		ball.transform.position = curPosition;
		*/
	}

	void OnMouseUp(){
		mouseSpeed = oldMouse - Input.mousePosition;
		mouseSpeed = new Vector3(mouseSpeed.x, mouseSpeed.y, mouseSpeed.y);
		ball.GetComponent<Rigidbody>().AddForce(mouseSpeed * speed * -1, ForceMode.Force);
		//rigidbody.AddForce(mouseSpeed*Time.deltaTime, ForceMode.Force);
	}
}
