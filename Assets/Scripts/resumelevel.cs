﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class resumelevel :MonoBehaviour , IPointerClickHandler
{
	public GameObject pausemenu, resetbutton, gameover, pauseleft, bestscoreback;

	public void OnPointerClick(PointerEventData data)
	{
		if(pausebutton.acik)
		{
			resetbutton.SetActive(true);
			gameover.SetActive(true);
		} else {
			bestscoreback.SetActive(false);
		}

		pausemenu.GetComponent<Animator>().SetBool("kapan", true);
		pauseleft.GetComponent<Animator>().SetBool("kapan", true);

		StartCoroutine(SetFalse());

		//pausemenu.SetActive(false);
		pausebutton.pressed = false;
		Time.timeScale = gameJump.timescale;
	}


	private void Update()
	{

	}

	IEnumerator SetFalse()
	{
		yield return new WaitForSeconds(0.1f);
		pausemenu.GetComponent<Animator>().SetBool("acil", false);
		pausemenu.GetComponent<Animator>().SetBool("kapan", false);
		pauseleft.GetComponent<Animator>().SetBool("acil", false);
		pauseleft.GetComponent<Animator>().SetBool("kapan", false);
	}
}
