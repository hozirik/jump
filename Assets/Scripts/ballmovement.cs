﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ballmovement : MonoBehaviour {

	public float velocity = 2.8f;
	private float sol, sag, solblock, sagblock, xyeni, zyeni;
	int i = 0;
	public GameObject block;
	public GameObject[] blockclone;
	public int clonenumber = 200;
	private Ray ray;
	private RaycastHit raycasthit;
	public GameObject scorelabel, bestscore, gameoverlabel, resetbutton, bestscoreback;
	public Sprite signin, signout;
	public GameObject signbutton;
	BinaryFormatter bf;
	FileStream file;
	PlayerData data;
	public GameObject coin, goldgain;
	int gold;
	public Color[] renk1, renk2, renk3, renk4, renk5, renk6, renk7, renk8, renk9, 
		renk10, renk11, renk12, renk13, renk14, renk15;
	Color[] renk;
	int c, d;
	int e = 1;
	bool renkbitti = false;

	public float dampTime = 0.5f;
	private Vector3 velocity3 = Vector3.zero;

	// Use this for initialization
	void Start () {
	
		PlayGamesPlatform.Activate();

		Social.localUser.Authenticate((bool success) => {

			if(success)
			{
				signbutton.GetComponent<Image>().sprite = signout;
			}else {
				signbutton.GetComponent<Image>().sprite = signin;
			}
		});

		GameObject.Find("block" + i).GetComponent<Rigidbody>().velocity = new Vector2(velocity, 0);

		blockclone = new GameObject[clonenumber];
		blockclone[0] = block;

		scorelabel.GetComponent<Text>().text = i.ToString();

		if(!File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
		{
			bf = new BinaryFormatter();
			file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

			data = new PlayerData();
			data.bestscore = 0;
			data.coin = 2000;

			bf.Serialize(file, data);
			file.Close();
		}

		bf = new BinaryFormatter();
		file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

		data = (PlayerData)bf.Deserialize(file);
		file.Close();

		gold = data.coin;

		coin.GetComponent<Text>().text = gold.ToString();

		c = UnityEngine.Random.Range(1, 16);

		switch (c)
		{
		case 1:	renk = renk1;
			break;
		case 2:	renk = renk2;
			break;
		case 3:	renk = renk3;
			break;
		case 4:	renk = renk4;
			break;
		case 5:	renk = renk5;
			break;
		case 6:	renk = renk6;
			break;
		case 7:	renk = renk7;
			break;
		case 8:	renk = renk8;
			break;
		case 9:	renk = renk9;
			break;
		case 10: renk = renk2;
			break;
		case 11: renk = renk1;
			break;
		case 12: renk = renk12;
			break;
		case 13: renk = renk13;
			break;
		case 14: renk = renk14;
			break;
		case 15: renk = renk15;
			break;
		}

		GameObject.Find("block" + i).GetComponent<Renderer>().material.SetColor("_Color", renk[1]);
		GameObject.Find("block" + i).GetComponent<Renderer>().material.SetColor("_EmissionColor", renk[1]);

		for(int b = 1; b < 5; ++b)
		{
			blockclone[i+b] = (GameObject)Instantiate(GameObject.Find("block" + (b-1)), new Vector3(0, 
				GameObject.Find("block" + (b-1)).transform.position.y, 
				GameObject.Find("block" + (b-1)).transform.position.z + 10.0f), Quaternion.identity);
			blockclone[i+b].name = "block" + (i+b);
			blockclone[i+b].GetComponent<Rigidbody>().velocity = new Vector3(velocity + (0.07f * (i+b)), 0, 0);
			blockclone[i+b].GetComponent<Renderer>().material.SetColor("_Color", renk[b+1]);
			blockclone[i+b].GetComponent<Renderer>().material.SetColor("_EmissionColor", renk[b+1]);
		}

		d = 5;
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 targetpos = new Vector3(0, 0, -5.0f + (i * 10));
		GameObject.Find("Main Camera").transform.position = Vector3.SmoothDamp(GameObject.Find("Main Camera").transform.position, 
			targetpos, ref velocity3, dampTime);
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name == "block" + (i+1))
		{
			gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

			AddScore();
		} else if(other.gameObject.name == "gameoversinir")
		{
			GameOver();
		}
	}

	void AddScore() {

		i += 1;

		if((d == 0 || d == 7) && renkbitti) {
			renkbitti = false;
			e *= -1;

			switch (c)
			{
			case 1:	renk = renk2; c = 2;
				break;
			case 2:	renk = renk3; c = 3;
				break;
			case 3:	renk = renk4; c = 4;
				break;
			case 4:	renk = renk5; c = 5;
				break;
			case 5:	renk = renk6; c = 6;
				break;
			case 6:	renk = renk15; c = 15;
				break;
			case 7:	renk = renk8; c = 8;
				break;
			case 8:	renk = renk9; c = 9;
				break;
			case 9:	renk = renk10; c = 10;
				break;
			case 10: renk = renk14; c = 14;
				break;
			case 11: renk = renk1; c = 1;
				break;
			case 12: renk = renk11; c = 11;
				break;
			case 13: renk = renk12; c = 12;
				break;
			case 14: renk = renk13; c = 13;
				break;
			case 15: renk = renk7; c = 7;
				break;
			}
		} else {
			d += e;

			if(d == 7 || d == 0) {
				renkbitti = true;
			}
		}

		goldgain.GetComponent<Animator>().SetBool("goldgain", true);
		StartCoroutine(SetAnimFalse());

		gold += 10;

		GameObject.Find("block" + i).GetComponentInChildren<Animation>().Play();

		bf = new BinaryFormatter();
		file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
		data = (PlayerData)bf.Deserialize(file);
		file.Close();

		int databestscore = data.bestscore;

		bf = new BinaryFormatter();
		file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

		data = new PlayerData();
		data.bestscore = databestscore;
		data.coin = gold;
		bf.Serialize(file, data);
		file.Close();

		coin.GetComponent<Text>().text = gold.ToString();
		scorelabel.GetComponent<Text>().text = i.ToString();
		
		blockclone[i+4] = (GameObject)Instantiate(GameObject.Find("block" + (i+3)), new Vector3(0, 
			GameObject.Find("block" + (i+3)).transform.position.y,
			GameObject.Find("block" + (i+3)).transform.position.z + 10.0f), Quaternion.identity);
		blockclone[i+4].name = "block" + (i+4);
		blockclone[i+4].GetComponent<Rigidbody>().velocity = new Vector3(velocity + (0.07f * (i+4)), 0, 0);

		blockclone[i+4].GetComponent<Renderer>().material.SetColor("_Color", renk[d]);
		blockclone[i+4].GetComponent<Renderer>().material.SetColor("_EmissionColor", renk[d]);
	}

	IEnumerator SetAnimFalse() {
		yield return new WaitForSeconds(0.1f);
		goldgain.GetComponent<Animator>().SetBool("goldgain", false);
	}

	void GameOver() {

		bf = new BinaryFormatter();
		file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

		data = (PlayerData)bf.Deserialize(file);
		file.Close();

		int databestscore = data.bestscore;

		if(i > data.bestscore)
		{
			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

			data = new PlayerData();
			data.bestscore = i;
			data.coin = gold;
			bf.Serialize(file, data);
			file.Close();
		} else 
		{
			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

			data = new PlayerData();
			data.bestscore = databestscore;
			data.coin = gold;
			bf.Serialize(file, data);
			file.Close();
		}

		if (Social.localUser.authenticated)
		{
			long leaderboardscore = Convert.ToInt64(i);
			Social.ReportScore(leaderboardscore, "CgkI7pbQs9MJEAIQAg", (bool success) => {

				if(success){
					print("oldu");
				}else {
					print("olmadı");
				}
			});
		}

		bestscore.GetComponent<Text>().text = "Best: " + data.bestscore;
		bestscoreback.SetActive(true);
		gameoverlabel.SetActive(true);
		resetbutton.SetActive(true);
	}
}

[Serializable]
class PlayerData
{
	public int bestscore;
	public int coin;
}